# Zadani
- minimálně 3 různé datasety, které tvoří jeden tématický celek (budou dále využity pro UC4 a UC5)
- alespoň jeden vstupní dataset bude mít nejméně 5.000 záznamů
- vybrané datové sady musí být navzájem propojitelné skrz nějaký společný identifikátor či jiný sloupec
- uvést zdroje odkud jsou jednotlivé datové sety čerpány
- uvést jeden tématický celek, jinými slovy porozumět vybraným datům a uvést jakou oblast řeší
- předložené datasety je nutné vybrat tak, aby bylo možné vytvářet smysluplné vizualizace
- uvést, co lze z dat získat
- uvést, jak rozumíte jednotlivým datovým sadám, například jakého datového typu je každý sloupec, co je obsahem každého sloupce a případná integritní omezení na sloupcích
- uvést, jakého datového formátu jsou datové sady

- Pro indexaci dat musí být použit Elasticsearch,
- data do Elasticsearch je nutné importovat pomocí Logstash,
- vizualizace dat musí být zpracována pomocí vizualizačních nástrojů v Kibaně.

# Reseni

Data pro zpracování pochází z webu - https://www.kaggle.com/arashnic/book-recommendation-dataset .
Tato data popisují knihy a jejích hodnocení různými uživateli.

## Dataset se skládá ze 3 souborů formátu csv:

### Users.csv 
[278 858 záznamů] [Prvotní zdroj: https://www.bookcrossing.com/]

Obsahuje informace o uživatelích ve 3 sloupcích:
1. User-ID = reprezentuje identifikátor uživatele [celé číslo]
2. Location = umístění uživatele [řetězec | NULL]
3. Age = věk uživatele [celé číslo | NULL]

Demografické údaje (Location, Age) jsou uvedeny jen tehdy, pokud jsou k dispozici. Sloupec Age
může obsahovat nepravdivou hodnotu zadanou uživatelem (vyskytují se záznamy s Age > 100
nebo Age = 0).


### Books.csv 
[271 360 záznamů] [Prvotní zdroj: Amazon Web Services]

Obsahuje informace o knihách v 5 sloupcích:
1. ISBN = reprezentuje identifikátor knihy (Číslice a velká písmena) [řetězec]
2. Book-Title = název knihy [řetězec]
3. Book-Author = autor knihy [řetězec]
4. Year-Of-Publication = rok publikace [celé číslo]
5. Publisher = vydavatel knihy [řetězec]


- Sloupce (Image-URL-S, Image-URL-M, Image-URL-L) obsahují odkazy na obrázky knih ve 3 velikostech – malé, střední, velké. Tyto sloupce budou vynechány.

- Původní soubor obsahoval drobné chyby – 3 záznamy s nevyplněným autorem (autor byl součásti názvu knihy). 4618 záznamů s hodnotou 0 v sloupci Year-Of-Publication.

- Nevyplněné hodnoty o autorech byly doplněny do souboru Books.csv. Data z tohoto souboru byla dále zpracována python scriptem – filter.py (filtrování sloupců + odstranění 4618 záznamů s hodnotou 0 v sloupci Year-Of-Publication). Finální data byla uložena do souboru Books_filtered.csv


## Ratings.csv 
[1 149 782 záznamů] [Prvotní zdroj: https://www.bookcrossing.com/]

Soubor propojuje jednotlivé uživatele a knihy na základě jejích hodnocení.
1. User-ID = odkazuje na uživatele ze souboru Users.csv [celé číslo]
2. ISBN = odkazuje na knihu ze souboru Books.csv [řetězec]
3. Book-Rating = hodnocení knihy. Buď explicitní – vyjádřené na stupnici od 1 do 10, nebo
implicitní – reprezentované hodnotou 0 [celé číslo]

Explicitní / Implicitní hodnocení
- Explicitní = číslo zadané přímo daným uživatelem (1–10)
- Implicitní = vyjadřuje možný zájem uživatele o danou knihu (například pomocí historie vyhledávání, kliknutí na odkazy, doby zobrazení stránek o dané knize či autorovi atd.)Analýza

## Z nalezených dat lze analyzovat následující:
- Srovnat hodnocení jednotlivých knih v závislosti na demografických údajích čtenáře (=uživatele) (věk či umístění).
- Porovnat explicitní a implicitní hodnocení knih
- Zjistit průměrné explicitní hodnocení jednotlivých knih
- Porovnat množství explicitních hodnocení v rámci jednotlivých věkových skupin
- Zjistit počet dostupných knih od jednotlivých vydavatelů / autorů
